---
title: "Preparing the SCF data"
author: "Fabian Greimel"
date: "25 September 2017"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Downloading the data

I downloaded the _summary extract (public) data_ from the [website of Fed Board of Governors](https://www.federalreserve.gov/econres/scfindex.htm).

The data sets were downloaded in Stata's dta-format and saved to the folder `raw-data`.

## Combining the data sets into one big data.table

Run the file `R/joining.R`. It will load the data sets into R, combine them and save them as `cleaned-data/scf_1989_2013.RData`
